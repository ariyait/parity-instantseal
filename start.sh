#!/bin/bash

RPCPORT=8545
WSPORT=8546
if [[ "${ROOT}" = "" ]] ; then ROOT="/geth" ; fi
if [[ "${UNLOCK_ACCOUNT}" = "" ]] ; then UNLOCK_ACCOUNT="8b5373835c707fa744eefa1e3e3b17bb119e409b" ; fi

source ./common_start.sh

node_start() {
  # launch parity in the background
  parity --config /parity/instant-seal-config.toml --tracing=on --fat-db=on --pruning=archive &
  NODE_PID=$!
}

start
